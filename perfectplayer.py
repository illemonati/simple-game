from game import Game, GameState


class PerfectPlayer:
    @staticmethod
    def act(state: GameState):
        if state == GameState.ROCK:
            return GameState.PAPER.value
        elif state == GameState.PAPER:
            return GameState.SCISSOR.value
        elif state == GameState.SCISSOR:
            return GameState.ROCK.value


def main():
    game = Game()
    player = PerfectPlayer()
    for i in range(0, 10):
        game.reset()
        action = PerfectPlayer.act(game.state)
        game.play(action)


if __name__ == '__main__':
    main()
