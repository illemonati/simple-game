from game import Game, GameState
import numpy as np
import random
from collections import deque
import tensorflow as tf


class DQNPlayer:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.epsilon = 1.0
        self.epsilon_decay = 0.98
        self.epsilon_min = 0.02
        self.alpha = 0.6
        self.memory = deque(maxlen=1000)
        self.batch_size = 100
        self.gamma = 0.6
        self.model = self.make_model()

    def make_model(self):
        inputs = tf.keras.layers.Input(shape=(self.state_size,))
        d1 = tf.keras.layers.Dense(32)(inputs)
        l_relu_1 = tf.keras.layers.LeakyReLU()(d1)
        d2 = tf.keras.layers.Dense(32)(l_relu_1)
        l_relu_2 = tf.keras.layers.LeakyReLU()(d2)
        d3 = tf.keras.layers.Dense(64)(l_relu_2)
        l_relu_3 = tf.keras.layers.LeakyReLU()(d3)
        outputs = tf.keras.layers.Dense(self.action_size, activation='softmax')(l_relu_3)
        model = tf.keras.Model(inputs, outputs)
        model.compile(optimizer=tf.keras.optimizers.Adam(lr=self.alpha),
                      loss=tf.keras.losses.categorical_crossentropy,
                      metrics=['acc'])
        model.summary()
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def batch_train(self):
        batch = random.sample(self.memory, self.batch_size) if len(self.memory) > self.batch_size else self.memory
        x = []
        y = []
        for state, action, reward, next_state, done in batch:
            target = 0
            if done:
                target = reward
            else:
                target = reward + self.gamma * np.amax(self.model.predict([next_state])[0])
            target_f = self.model.predict([state])[0]
            target_f[action] = target
            x.append(state[0])
            y.append(target_f)

        self.epsilon = max(self.epsilon * self.epsilon_decay, self.epsilon_min)
        # print(x)
        # print(y)
        self.model.fit(np.array(x), np.array(y), epochs=5, verbose=1)

    def act(self, state, train=True):
        if random.random() < self.epsilon and train:
            return random.choice(range(self.action_size))
        else:
            return np.argmax(self.model.predict(state)[0])


def one_hot_encode(state, size):
    one_hot_state = np.zeros((size, ))
    one_hot_state[state] = 1
    one_hot_state = np.reshape(one_hot_state, (1, size))
    return one_hot_state


def train(player: DQNPlayer, game: Game, eps: int):
    for ep in range(eps):
        game.reset()
        state = one_hot_encode(game.state.value, game.state_size)
        action = player.act(state)
        game.play(action, False)
        next_state = game.state
        done = True
        reward = 0
        if next_state == GameState.WIN:
            reward = 1
        elif next_state == GameState.LOSE:
            reward = 0
        player.remember(state, action, reward, one_hot_encode(next_state.value, game.state_size), done)
        if ep % 20 == 0:
            print(f'ep: {ep}, epsilon: {player.epsilon}')
            player.batch_train()
            print(player.model.predict([[1, 0, 0, 0, 0, 0]]))
            print(player.model.predict([[0, 1, 0, 0, 0, 0]]))
            print(player.model.predict([[0, 0, 1, 0, 0, 0]]))


def test(player: DQNPlayer, game: Game, eps: int):
    for ep in range(eps):
        game.reset()
        state = game.state.value
        action = player.act(one_hot_encode(state, game.state_size), False)
        game.play(action)


def main():
    game = Game()
    player = DQNPlayer(game.state_size, game.action_size)
    train(player, game, 250)
    test(player, game, 10)
    player.model.save('model.h5')
    layer_save = open('layer_save.txt', 'w')
    np.set_printoptions(threshold=np.inf)
    for layer in player.model.layers:
        layer_save.write(f'{layer.get_weights()}\n')
    layer_save.close()


if __name__ == '__main__':
    main()
