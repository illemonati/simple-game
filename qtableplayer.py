from game import Game, GameState
import numpy as np
import random
from collections import deque


class QTablePlayer:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.q_table = np.zeros([state_size, action_size])
        self.epsilon = 1.0
        self.epsilon_decay = 0.995
        self.epsilon_min = 0.02
        self.alpha = 0.8
        self.memory = deque(maxlen=100)
        self.batch_size = 10
        self.gamma = 0.6

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def batch_train(self):
        batch = random.sample(self.memory, self.batch_size) if len(self.memory) > self.batch_size else self.memory
        for state, action, reward, next_state, done in batch:
            if not done:
                self.q_table[state, action] += self.alpha * \
                                               (reward + self.gamma * np.amax(next_state) - self.q_table[state, action])
            else:
                self.q_table[state, action] = reward
        self.epsilon = max(self.epsilon * self.epsilon_decay, self.epsilon_min)

    def act(self, state, train=True):
        if random.random() < self.epsilon and train:
            return random.choice(range(self.action_size))
        else:
            return np.argmax(self.q_table[state])


def train(player: QTablePlayer, game: Game, eps: int):
    for ep in range(eps):
        game.reset()
        state = game.state.value
        action = player.act(state)
        game.play(action, False)
        next_state = game.state
        done = True
        reward = 0
        if next_state == GameState.WIN:
            reward = 1
        elif next_state == GameState.LOSE:
            reward = -1
        player.remember(state, action, reward, next_state, done)
        if ep % 20 == 0:
            player.batch_train()


def test(player: QTablePlayer, game: Game, eps: int):
    for ep in range(eps):
        game.reset()
        state = game.state.value
        action = player.act(state, False)
        game.play(action)


def main():
    game = Game()
    player = QTablePlayer(game.state_size, game.action_size)
    train(player, game, 100)
    print(player.q_table)
    test(player, game, 10)


if __name__ == '__main__':
    main()


