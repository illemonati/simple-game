from game import Game


def main():
    game = Game()
    while True:
        game.reset()
        print("Gamed Played: ", game.state)
        player_input = input("Your input: ")
        game.play(int(player_input))


if __name__ == '__main__':
    main()
