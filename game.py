import random

from enum import Enum


class GameState(Enum):
    ROCK = 0
    PAPER = 1
    SCISSOR = 2
    WIN = 3
    LOSE = 4
    TIE = 5


class Game:
    def __init__(self):
        self.state = None
        self.reset()
        self.state_size = 6
        self.action_size = 3

    def reset(self):
        possible_starter_states = [GameState.ROCK, GameState.PAPER, GameState.SCISSOR]
        self.state = random.choice(possible_starter_states)

    def play(self, action, verbose=True):
        if verbose:
            print(f"Game Played: {self.state.name}, Player Played: {GameState(action).name}, ", end='')
        wintable = {
            GameState.ROCK: GameState.PAPER,
            GameState.PAPER: GameState.SCISSOR,
            GameState.SCISSOR: GameState.ROCK
        }
        if action == self.state.value:
            self.state = GameState.TIE
        elif wintable[self.state].value == action:
            self.state = GameState.WIN
        else:
            self.state = GameState.LOSE
        if verbose:
            print(f'Player {self.state.name}')
